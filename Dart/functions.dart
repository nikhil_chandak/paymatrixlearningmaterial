void main() {
  showOutput(square(2));
  showOutput(square(2.5));

  // Anonymous Function (Lambda or closure function)

  var list = ['apples', 'bananas', 'oranges'];
  list.forEach((item) {
    print('${list.indexOf(item)}: $item');
  });


  // Named parameter being optional 
  
  print(sum(2, num2: 2));
  print(sum(2));

  /*
  We can annotate a named parameter in any Dart code (not just Flutter) with @required to indicate that it is a required parameter.
  const Scrollbar({Key key, @required Widget child})
  Required is defined in the meta package. Either import package:meta/meta.dart directly, 
  or import another package that exports meta, such as Flutter’s package:flutter/material.dart. */

  print(sum2(2, 2));
  print(sum2(2));

}

void showOutput(var msg) {
  print(msg);
}

// arrow function 
dynamic square(var num) => num * num;

// mixing position and named parameter 
dynamic sum(var num1, {var num2}) => num1 + ( num2 ?? 0 );

// position optional paramter 
dynamic sum2(var num1, [var num2]) => num1 + ( num2 ?? 0 );