class Num {
  int num = 10;
}

main () {
  var n = Num();
  int number;
  
  number = n?.num; // null aware
  print(number);

  // null aware variation
  var k;
  int number2 = k?.num ?? 18;
  print(number2);
  
  int number3 ; 
  print(number3 ??= 70);
}