
void main() {
  var s1 = 'Single quotes work well for string literals.';
  var s2 = "Double quotes work just as well.";
  var s3 = 'It\'s easy to escape the string delimiter.';
  var s4 = "It's even easier to use the other delimiter.";

  // I was not aware of the concept of raw strings. Interesting! 
  var s = r'In a raw string, not even \n gets special treatment.';

  var t1 = '''
  You can create
  multi-line strings like this one.
  ''';

  var t2 = """This is also a
  multi-line string.""";

  print(s1);
  print(s2);
  print(s3);
  print(s4);
  print(s);
  print(t1);
  print(t2);

}