import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:weather_app/model/weatherModel.dart';
import 'package:weather_app/repository/weatherRepo.dart';
import 'package:weather_app/repository/forecastRepo.dart';
import 'package:weather_app/model/forecastModel.dart';

class WeatherEvent extends Equatable{
  @override
  // TODO: implement props
  List<Object> get props => [];

}

class FetchWeather extends WeatherEvent{
  final _lat;
  final _lon;

  FetchWeather(this._lat, this._lon);

  @override
  // TODO: implement props
  List<Object> get props => [_lat, _lon];
}

class ResetWeather extends WeatherEvent{

}

class WeatherState extends Equatable{
  @override
  // TODO: implement props
  List<Object> get props => [];

}


class WeatherIsLoading extends WeatherState{

}

class WeatherIsLoaded extends WeatherState{
  final _weather;
  final _forecast;

  WeatherIsLoaded(this._weather, this._forecast);

  WeatherData get getWeather => _weather;
  ForecastData get getForecast => _forecast;

  @override
  // TODO: implement props
  List<Object> get props => [_weather, _forecast];
}

class WeatherIsNotLoaded extends WeatherState{

}

class WeatherBloc extends Bloc<WeatherEvent, WeatherState>{

  WeatherRepo weatherRepo;
  ForecastRepo forecastRepo;

  WeatherBloc(this.weatherRepo, this.forecastRepo);

  @override
  // TODO: implement initialState
  WeatherState get initialState => WeatherIsNotLoaded();

  @override
  Stream<WeatherState> mapEventToState(WeatherEvent event) async*{
    // TODO: implement mapEventToState
    if(event is FetchWeather){
      yield WeatherIsLoading();

      try{
        // WeatherData weather = await weatherRepo.getWeather(event._lat, event._lon) ;
        WeatherData weather = (await weatherRepo.getWeather(event._lat, event._lon)) as WeatherData;
        ForecastData forecast = await forecastRepo.getForecast(event._lat, event._lon) ;
        yield WeatherIsLoaded(weather, forecast);
      }catch(_){
        print(_);
        yield WeatherIsNotLoaded();
      }
    }else if(event is ResetWeather){
      yield WeatherIsNotLoaded();
    }
  }

}