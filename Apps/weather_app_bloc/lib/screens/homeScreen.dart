// import 'dart:convert';
// import 'package:flutter/material.dart';
// import 'package:geolocator/geolocator.dart';
// import 'package:weather_app/model/forecastModel.dart';
// import 'package:weather_app/model/weatherModel.dart';
// import 'package:weather_app/widgets/weather.dart';
// import 'package:weather_app/widgets/weatherItem.dart';
// import 'package:http/http.dart' as http;

// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:weather_app/repository/weatherRepo.dart';
// import 'package:weather_app/repository/forecastRepo.dart';
// import 'package:weather_app/bloc/weatherBloc.dart';

// class Homescreen extends StatefulWidget {
//   @override
//   _HomescreenState createState() => _HomescreenState();
// }

// class _HomescreenState extends State<Homescreen> {
//   bool isLoading = false;
//   WeatherData weatherData;
//   ForecastData forecastData;
//   final lat = 22.572645 ;
//   final lon = 88.363892 ;

//   loadWeather() async {
//     setState(() {
//       isLoading = true;
//     });
//     Position position;
//     try {
//       print(2);
//     } catch (e) {
//       print(e);
//     }

//     if (position == null) {
//       print("in");
//       // final lat = position.latitude;
//       // final lon = position.longitude;

//       final weatherResponse = await http.get(
//           'https://api.openweathermap.org/data/2.5/weather?APPID=0f9fbe9b7a2280d461c28a93b005816c&lat=${lat.toString()}&lon=${lon.toString()}');
//       final forecastResponse = await http.get(
//           'https://api.openweathermap.org/data/2.5/forecast?APPID=0f9fbe9b7a2280d461c28a93b005816c&lat=${lat.toString()}&lon=${lon.toString()}');

//       if (weatherResponse.statusCode == 200 &&
//           forecastResponse.statusCode == 200) {
//         return setState(() {
//           weatherData =
//               new WeatherData.fromJson(jsonDecode(weatherResponse.body));
//           forecastData =
//               new ForecastData.fromJson(jsonDecode(forecastResponse.body));
//           isLoading = false;
//         });
//       }
//     }

//     setState(() {
//       isLoading = false;
//     });
//   }

//   @override
//   void initState() {
//     super.initState();
//     // loadWeather();
//   }

//   @override
//   Widget build(BuildContext context) {
//       // print("12");
//       // final weatherBloc = BlocProvider.of<WeatherBloc>(context);
//       // print("123");
//       bool isLoading = false ;
//       WeatherData weatherData;
//       ForecastData forecastData;

//       BlocBuilder<WeatherBloc, WeatherState>(
//       builder: (context, state){
//           if(state is WeatherIsNotLoaded) {
//             isLoading = false ;
//             BlocProvider.of<WeatherBloc>(context).add.add(FetchWeather(lat, lon));
//             weatherData = null ;
//             forecastData = null ;
//             return Text("Error", style: TextStyle(color: Colors.white),);
//           }
//           else if(state is WeatherIsLoading) {
//             isLoading = true ;
//           }
//           else if(state is WeatherIsLoaded) {
//             isLoading = false ;
//             weatherData = state.getWeather ;
//             forecastData = state.getForecast ;
//           }
//           return ShowWeather(isLoading, weatherData, forecastData);
//           return Text("Oo ops!");
//         }
//       );
//       // return Text("Oo ops!");
//   }
// }


// class ShowWeather extends StatelessWidget {
//   final lat = 22.572645 ;
//   final lon = 88.363892 ;
//   final bool isLoading ;
//   final WeatherData weatherData;
//   final ForecastData forecastData;

//   ShowWeather(this.isLoading, this.weatherData, this.forecastData);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('Current Weather'),
//       ),
//       body: Center(
//         child: Column(
//           mainAxisSize: MainAxisSize.min,
//           children: <Widget>[
//             Expanded(
//               child: Column(
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 children: <Widget>[
//                   Padding(
//                     padding: const EdgeInsets.all(8.0),
//                     child: weatherData != null
//                         ? Weather(weather: weatherData)
//                         : Container(),
//                   ),
//                   Padding(
//                     padding: const EdgeInsets.all(8.0),
//                     child: isLoading
//                         ? CircularProgressIndicator(
//                       strokeWidth: 2.0,
//                       valueColor:
//                       new AlwaysStoppedAnimation(Colors.white),
//                     )
//                         : IconButton(
//                       icon: new Icon(Icons.refresh),
//                       tooltip: 'Refresh',
//                       onPressed: () {
//                         BlocProvider.of<WeatherBloc>(context).add(FetchWeather(lat, lon));
//                       },
//                       color: Colors.white,
//                     ),
//                   ),
//                 ],
//               ),
//             ),
//             SafeArea(
//               child: Padding(
//                 padding: const EdgeInsets.all(8.0),
//                 child: Container(
//                   height: 200.0,
//                   child: forecastData != null
//                       ? ListView.builder(
//                       itemCount: forecastData.list.length,
//                       scrollDirection: Axis.horizontal,
//                       itemBuilder: (context, index) => WeatherItem(
//                           weather: forecastData.list.elementAt(index)))
//                       : Container(),
//                 ),
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }




import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:weather_app/model/forecastModel.dart';
import 'package:weather_app/model/weatherModel.dart';
import 'package:weather_app/widgets/weather.dart';
import 'package:weather_app/widgets/weatherItem.dart';
import 'package:http/http.dart' as http;

class Homescreen extends StatefulWidget {
  @override
  _HomescreenState createState() => _HomescreenState();
}

class _HomescreenState extends State<Homescreen> {
  bool isLoading = false;
  WeatherData weatherData;
  ForecastData forecastData;

  loadWeather() async {
    setState(() {
      isLoading = true;
    });
    Position position;
    try {
      print(1);
      // final Geolocator geolocator = Geolocator()..forceAndroidLocationManager = true;
      // position =
      //     await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.medium);
      // final position = await Geolocator.getCurrentPosition(forceAndroidLocationManager: true);
      // position = await Geolocator.getCurrentPosition(
          // desiredAccuracy: LocationAccuracy.low,forceAndroidLocationManager: true);
      print(2);
    } catch (e) {
      print(e);
    }

    if (position == null) {
      print("in");
      final lat = 22.572645 ;
      final lon = 88.363892 ;
      // final lat = position.latitude;
      // final lon = position.longitude;

      final weatherResponse = await http.get(
          'https://api.openweathermap.org/data/2.5/weather?APPID=0f9fbe9b7a2280d461c28a93b005816c&lat=${lat.toString()}&lon=${lon.toString()}');
      final forecastResponse = await http.get(
          'https://api.openweathermap.org/data/2.5/forecast?APPID=0f9fbe9b7a2280d461c28a93b005816c&lat=${lat.toString()}&lon=${lon.toString()}');

      if (weatherResponse.statusCode == 200 &&
          forecastResponse.statusCode == 200) {
        return setState(() {
          weatherData =
              new WeatherData.fromJson(jsonDecode(weatherResponse.body));
          forecastData =
              new ForecastData.fromJson(jsonDecode(forecastResponse.body));
          isLoading = false;
        });
      }
    }

    setState(() {
      isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    loadWeather();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Current Weather'),
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: weatherData != null
                        ? Weather(weather: weatherData)
                        : Container(),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: isLoading
                        ? CircularProgressIndicator(
                            strokeWidth: 2.0,
                            valueColor:
                                new AlwaysStoppedAnimation(Colors.white),
                          )
                        : IconButton(
                            icon: new Icon(Icons.refresh),
                            tooltip: 'Refresh',
                            onPressed: loadWeather,
                            color: Colors.white,
                          ),
                  ),
                ],
              ),
            ),
            SafeArea(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  height: 200.0,
                  child: forecastData != null
                      ? ListView.builder(
                          itemCount: forecastData.list.length,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) => WeatherItem(
                              weather: forecastData.list.elementAt(index)))
                      : Container(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

 