import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:weather_app/screens/splahScreen.dart';
import 'package:weather_app/theme/theme.dart';

import 'theme/theme.dart';
import 'theme/theme.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/bloc/weatherBloc.dart';
import 'package:weather_app/repository/forecastRepo.dart';
import 'package:weather_app/repository/weatherRepo.dart';

void main() {
  runApp(WeatherApp());
}

class WeatherApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Weather App',
      debugShowCheckedModeBanner: false,
      theme: darktheme(),
      darkTheme: lighttheme(),
      themeMode: ThemeMode.system,
      // home: Splashscreen(),

      home: BlocProvider(
      builder: (context) => WeatherBloc(WeatherRepo(), ForecastRepo()),
      child: Splashscreen(),
      ),
    );
  }
}
