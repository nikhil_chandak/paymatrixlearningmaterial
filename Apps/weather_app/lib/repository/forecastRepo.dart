import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:weather_app/model/forecastModel.dart';

class ForecastRepo{
  Future<ForecastData> getForecast(final lat, final lon) async{
    // final result = await http.Client().get("https://api.openweathermap.org/data/2.5/weather?q=$city&APPID=43ea6baaad7663dc17637e22ee6f78f2");

    final result = await http.get(
        'https://api.openweathermap.org/data/2.5/forecast?APPID=0f9fbe9b7a2280d461c28a93b005816c&lat=${lat.toString()}&lon=${lon.toString()}');

    if(result.statusCode != 200)
      throw Exception();

    return parsedJson(result.body);
  }

  ForecastData parsedJson(final response){
    final jsonDecoded = json.decode(response);

    // final jsonWeather = jsonDecoded["main"];
    final jsonWeather = jsonDecoded ;

    return ForecastData.fromJson(jsonWeather);
  }
}